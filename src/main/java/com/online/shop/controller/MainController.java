package com.online.shop.controller;

import com.online.shop.dto.ProductDto;
import com.online.shop.service.ProductService;
import com.online.shop.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductValidator productValidator;

    @GetMapping("/addProduct")
    public String addProductPageGet(Model model){
        // se va executa "business logic" :)
        // după care întoarcem un nume de pagină

        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPagePost(@ModelAttribute ProductDto productDto, BindingResult bindingResult, @RequestParam("productImage") MultipartFile multipartFile) throws IOException {
        System.out.println(multipartFile.getBytes());
        productValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()){
            return "addProduct";  // fără acel redirect:/ (de mai jos) întoarce aceeași pagină cu câmpurile
            // completate așa cum le-a băgat utilizatorul
        }
        productService.addProduct(productDto, multipartFile);
        return "redirect:/addProduct"; // cu acest redirect:/ întoarce aceeași pagină cu câmpurile goale
    }

    @GetMapping("/home")
    public String homepageGet(Model model){
        List<ProductDto> productDtoList = productService.getAllProductDto();
        model.addAttribute("productDtoList", productDtoList);
        System.out.println(productDtoList);
        return "homepage";
    }

    @GetMapping("/product/{productId}")
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model){
        Optional<ProductDto> optionalProductDto = productService.getProductDtoFromOptionalByID(productId);
        if (optionalProductDto.isEmpty()){
            return "error";
        }
        ProductDto productDto = optionalProductDto.get();
        model.addAttribute("productDtoSINGLE", productDto);
        System.out.println("Am dat click pe produsul cu ID-ul " + productId);
        return "viewProduct";
    }


}
