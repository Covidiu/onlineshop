package com.online.shop.service;

import com.online.shop.dto.UserDto;
import com.online.shop.entities.User;
import com.online.shop.mapper.UserMapper;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public void addUser(UserDto userDto){
        User user = userMapper.map(userDto);
        userRepository.save(user);
    }
}
